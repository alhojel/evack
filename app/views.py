# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
import requests
from bs4 import BeautifulSoup
import geocoder
from scipy import spatial
from huey import SqliteHuey
from huey import crontab
import huey
from twilio.rest import Client
from app.models import Profile

@login_required(login_url="/login/")
def index(request):

    url = "https://api.rss2json.com/v1/api.json?rss_url=http%3A%2F%2Fwww.gdacs.org%2Fxml%2Frss_24h.xml&api_key=ta13y1xnygsw9arkfnvfatgfl7jaddeq2l3jidbm&count=30"

    
    r = requests.get(url)
    near = requests.get("https://www.gdacs.org/xml/rss_24h.xml")

    data = r.json()
    data_items = data["items"]
    for cat in data_items:
        cat['categories'][0].replace("[]''",'')
        cat['link'] = cat['link'].replace('amp;','')
        head, sep, tail = cat['pubDate'].partition(' ')
        cat['pubDate'] = head

    
    soup = BeautifulSoup(near.content, 'html.parser')
    soups_lat = soup.find_all('geo:lat')
    soups_long = soup.find_all('geo:long')
    points_lat = []
    points_long = []
    for pnt in soups_lat:
        points_lat.extend(pnt)
    for pnt in soups_long:
        points_long.extend(pnt) 
    g = geocoder.ip('me')
    me_lat = g.lat
    me_long = g.lng
    #print(me_long,me_lat)
    points_lat = [float(i) for i in points_lat]
    points_long = [float(i) for i in points_long]
    #print(points_lat,points_long)

    tree = spatial.KDTree(list(zip(points_lat,points_long)))
    closest = tree.query([me_lat,me_long])
    closest_disaster = data_items[closest[1]]
    print(closest_disaster)
    
   # for pnt in points_long:
   #     print(type(pnt))
   # pnt = pnt.replace("<geo:long>/'",'')
   # for pint in points_lat:
    #    pint = pint.replace("<geo:lat>/'",'')
   # print(points_lat[3],points_long[3])


        # write the path to a local file with a JSON named map template
    

    return render(request, "index.html", {'data_items': data_items, 'closest_disaster':closest_disaster})






@login_required(login_url="/login/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]
        template = loader.get_template('pages/' + load_template)

        return HttpResponse(template.render(context, request))

    except:

        template = loader.get_template( 'pages/error-404.html' )
        return HttpResponse(template.render(context, request))





@login_required(login_url="/login/")
def sms(request):
    
    prof = Profile.objects.get(user=request.user)
    account_sid = "ACa9874c600c672936da966506179b0e94"
    auth_token  = "c68c60a49845727f3db1597b6376db5d"

    client = Client(account_sid, auth_token)
    cont = str(prof.country_code)
    phn = str(prof.phone_number)
    address = '+'+cont+phn
    message = client.messages.create(
        to=address, 
        from_="+12055516055",
        body="Test alarm from Evack!")

    return redirect('user')
