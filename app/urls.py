# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.urls import path, re_path
from app import views
from authentication.views import user

urlpatterns = [
    # Matches any html file 

    path('page-user', user, name='user'),
    re_path(r'^.*\.html', views.pages, name='pages'),
    # The home page
    path('', views.index, name='home'),
    path('test', views.sms, name='sms'),

]
