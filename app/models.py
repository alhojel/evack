# -*- encoding: utf-8 -*-
"""
License: MIT
Copyright (c) 2019 - present AppSeed.us
"""

from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number_verified = models.BooleanField(default=False)
    phone_number = models.IntegerField(unique=True)
    country_code = models.IntegerField()

    def __str__(self):
        return str(self.phone_number)
